require 'spec_helper'

require 'gitlab/triage/filter_builders/multi_filter_builder'

describe Gitlab::Triage::FilterBuilders::MultiFilterBuilder do
  let(:multi_filters) do
    {
      'labels' => {
        filter_content: %w[label1 label2],
        separator: ','
      }
    }
  end

  context '#build_filter' do
    it 'should build the correct filter string' do
      multi_filters.each do |k, v|
        builder = described_class.new(k, v[:filter_content], v[:separator])
        expect(builder.build_filter).to eq("&#{k}=#{v[:filter_content][0]}#{v[:separator]}#{v[:filter_content][1]}")
      end
    end
  end
end
