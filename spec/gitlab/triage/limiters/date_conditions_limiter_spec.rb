require 'spec_helper'

require 'gitlab/triage/limiters/date_conditions_limiter'

describe Gitlab::Triage::Limiters::DateConditionsLimiter do
  let(:created_at) { Date.new(2016, 1, 31) }
  let(:updated_at) { Date.new(2017, 1, 1) }

  let(:resource) do
    {
      created_at: created_at,
      updated_at: updated_at
    }
  end
  let(:condition) do
    {
      attribute: 'updated_at',
      condition: 'older_than',
      interval_type: 'months',
      interval: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a limiter'

  context '#resource_value' do
    it 'has the correct value for updated_at attribute' do
      expect(subject.resource_value).to eq(updated_at)
    end

    it 'has the correct value for the created_at attribute' do
      limiter = described_class.new(resource, condition.merge(attribute: 'created_at'))
      expect(limiter.resource_value).to eq(created_at)
    end
  end

  context '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(3.months.ago.to_date)
    end
  end

  context '#calculate' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to eq(true)
    end

    it 'calculate false given wrong condition' do
      limiter = described_class.new(resource, condition.merge(condition: 'newer_than'))
      expect(limiter.calculate).to eq(false)
    end
  end
end
