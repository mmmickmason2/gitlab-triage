require 'spec_helper'

require 'gitlab/triage/network'

describe Gitlab::Triage::Network do
  let(:token) { 'token' }
  let(:headers) do
    {
      'Content-type' => 'application/json',
      'PRIVATE-TOKEN' => token
    }
  end
  let(:network_adapter) { double }

  subject { described_class.new(network_adapter) }

  context '::query_api' do
    let(:results) { [{ a: 'b' }, { c: 'd' }] }
    let(:response) do
      {
        results: results,
        more_pages: false,
        next_page_url: ''
      }
    end
    let(:query_url) { 'https://gitlab.com/v4/api/projects/project/issues' }

    it 'acquires the correct results' do
      expect(network_adapter).to receive(:get).with(token, query_url).and_return(response)
      expect(subject.query_api(token, query_url)).to eq(results.map(&:with_indifferent_access))
    end

    it 'returns an empty array upon Net::ReadTimeout from adapter' do
      allow(network_adapter).to receive(:get).with(token, query_url).and_raise(Net::ReadTimeout)

      expect(subject.query_api(token, query_url)).to eq([])
    end
  end

  context '::post_api' do
    let(:post_url) { 'https://gitlab.com/v4/api/projects/project/issues' }
    let(:body) { 'body' }

    it 'calls the library' do
      expect(network_adapter).to receive(:post).with(token, post_url, body)

      subject.post_api(token, post_url, body)
    end

    it 'returns false upon Net::ReadTimeout from adapter' do
      allow(network_adapter).to receive(:post).with(token, post_url, body).and_raise(Net::ReadTimeout)

      expect(subject.post_api(token, post_url, body)).to eq(false)
    end
  end
end
