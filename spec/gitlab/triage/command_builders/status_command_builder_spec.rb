require 'spec_helper'

require 'gitlab/triage/command_builders/status_command_builder'

describe Gitlab::Triage::CommandBuilders::StatusCommandBuilder do
  let(:states) do
    %w[
      close
      reopen
    ]
  end

  context '#build_command' do
    it 'outputs the correct command' do
      states.each do |state|
        builder = described_class.new(state)
        expect(builder.build_command).to eq("/#{state}")
      end
    end

    it 'outputs an empty string if no command is present' do
      builder = described_class.new("")
      expect(builder.build_command).to eq("")

      builder = described_class.new(nil)
      expect(builder.build_command).to eq("")
    end
  end
end
