RSpec.shared_examples 'a limiter', :resource, :condition do
  context 'validation' do
    it 'throws an exception with an invalid condition parameters' do
      described_class.params_checking_condition_value.each do |field|
        expect do
          described_class.new(resource, condition.merge(field => 'invalid'))
        end.to raise_error(ArgumentError)
      end
    end

    it 'throws an exception with a missing condition parameters' do
      described_class.all_params_limiter_names.each do |field|
        tmp_condition = condition.dup
        tmp_condition.delete(field)
        expect do
          described_class.new(resource, tmp_condition)
        end.to raise_error(ArgumentError)
      end
    end

    it 'throws an exception given the wrong type for the condition parameter' do
      class InvalidType; end

      described_class.params_checking_condition_type.each do |field|
        expect do
          described_class.new(resource, condition.merge(field => InvalidType.new))
        end.to raise_error(ArgumentError)
      end
    end
  end
end
