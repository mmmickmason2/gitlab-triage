[![pipeline status](https://gitlab.com/gitlab-org/triage/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/triage/commits/master)

# GitLab Triage Project

This project contains the library and pipeline definition to enable automated triaging of issues in the [GitLab-CE Project](https://gitlab.com/gitlab-org/gitlab-ce).

## gitlab-triage gem

### Summary

The `gitlab-triage` gem aims to enable project managers and maintainers to automatically triage Issues and Merge Requests in GitLab projects based on defined policies.

### What is a triage policy?

Triage policies are defined on a resource level basis, resources being:
- Issues
- Merge Requests

Each policy can declare a number of conditions that must all be satisfied before a number of actions are carried out.

### Defining a policy

Policies are defined in a policy file (by default [.triage-policies.yml](.triage-policies.yml)). The format of the file is [YAML](https://en.wikipedia.org/wiki/YAML).

> Note: You can use the [`--init`](#usage) option to add an example [`.triage-policies.yml` file](support/.triage-policies.example.yml) to your project

Select which resource to add the policy to:
- `issues`
- `merge_requests`

And create an array of `rules` to define your policies:

For example:

```yml
resource_rules:
  issues:
    rules:
      - name: My policy
        conditions:
          date:
            attribute: updated_at
            condition: older_than
            interval_type: days
            interval: 5
          state: opened
          label:
            - No label
        actions:
          labels:
            - needs attention
          mention:
            - markglenfletcher
          comment: |
            This issue is unlabelled after 5 days. It needs attention.
  merge_requests:
    rules:
      []
```

### Fields

A policy consists of the following fields:
- [Name field](#name-field)
- [Conditions field](#conditions-field)
- [Actions field](#actions-field)

#### Name field

The name field is used to describe the purpose of the individual policy.

Example:

```yml
name: Policy name
```

#### Conditions field

Used to declare a condition that must be satisfied by a resource before actions will be taken.

Available condition types:
- [`date` condition](#date-condition)
- [`milestone` condition](#milestone-condition)
- [`state` condition](#state-condition)
- [`upvotes` condition](#upvotes-condition)
- [`labels` condition](#labels-condition)
- [`forbidden_labels` condition](#forbidden-labels-condition)

##### Date condition

Accepts a hash of fields.

| Field           | Type    | Values                              | Required  |
| ---------       | ----    | ----                                | --------  |
| `attribute`     | string  | `created_at`, `updated_at`          | yes       |
| `condition`     | string  | `older_than`, `newer_than`          | yes       |
| `interval_type` | string  | `days`, `weeks`, `months`, `years`  | yes       |
| `interval`      | integer | integer                             | yes       |

Example:

```yml
conditions:
  date:
    attribute: updated_at
    condition: older_than
    interval_type: months
    interval: 12
```

##### Milestone condition

Accepts an array of strings. Each element is the name of a milestone to filter upon.

> Note: **All** specified milestones must be present on the resource for the condition to be satisfied

Example:

```yml
conditions:
  milestone:
    - v1
    - v2
```

##### State condition

Accepts a string.

| State           | Type    | Value     |
| ---------       | ----    | ------    |
| Closed issues   | string  | `closed`  |
| Open issues     | string  | `opened`  |

Example:

```yml
conditions:
  state: opened
```

##### Upvotes condition

Accepts a hash of fields.

| Field           | Type    | Values                              | Required  |
| ---------       | ----    | ----                                | --------  |
| `attribute`     | string  | `upvotes`, `downvotes`              | yes       |
| `condition`     | string  | `less_than`, `greater_than`         | yes       |
| `threshold`     | integer | integer                             | yes       |

Example:

```yml
conditions:
  upvotes:
    attribute: upvotes
    condition: less_than
    threshold: 10
```

##### Labels condition

Accepts an array of strings. Each element in the array represents the name of a label to filter on.

> Note: **All** specified labels must be present on the resource for the condition to be satisfied

Example:

```yml
conditions:
  labels:
    - feature proposal
```

##### Forbidden labels condition

Accepts an array of strings. Each element in the array represents the name of a label to filter on.

> Note: **All** specified labels must be absent on the resource for the condition to be satisfied

Example:

```yml
conditions:
  forbidden_labels:
    - awaiting feedback
```

#### Actions field

Used to declare an action to be carried out on a resource if **all** conditions are satisfied.

Available action types:
- [`labels` action](#labels-action)
- [`status` action](#status-action)
- [`mention` action](#mention-action)
- [`comment` action](#comment-action)

##### Labels action

Adds a number of labels to the resource.

Accepts an array of strings. Each element is the name of a label to add.

Example:

```yml
actions:
  labels:
    - feature proposal
    - awaiting feedback
```

##### Status action

Changes the status of the resource.

Accepts a string.

| State transition    | Type    | Value     |
| ---------           | ----    | ------    |
| Close the resource  | string  | `close`   |
| Reopen the resource | string  | `reopen`  |

Example:

```yml
actions:
  status: close
```

##### Mention action

Mentions a number of users.

Accepts an array of strings. Each element is the username of a user to mention.

Example:

```yml
actions:
  mention:
    - rymai
    - markglenfletcher
```

##### Comment action

Adds a comment to the resource.

Accepts a string.

Example:

```yml
actions:
  comment: |
    Closing this issue automatically
```

### Usage

```
Usage: gitlab-triage [options]

    -n, --dry-run                    Don't actually update anything, just print
    -f, --policies-file [string]     A valid policies YML file
    -p, --project-id [string]        A project ID or path
    -t, --token [string]             A valid API token
    -d, --debug                      Print debug information
    -h, --help                       Print help message
        --init                       Initialize the project with a policy file
        --init-ci                    Initialize the project with a .gitlab-ci.yml file
```

#### Local

```
gem install gitlab-triage
gitlab-triage --help
gitlab-triage --dry-run --token $API_TOKEN --project-id gitlab-org/triage
```

#### GitLab CI pipeline

You can enforce policies using a scheduled pipeline:

```yml
run:triage:triage:
  stage: triage
  script:
    - gem install gitlab-triage
    - gitlab-triage --token $API_TOKEN --project-id $CI_PROJECT_PATH
  only:
    - schedules
```

> Note: You can use the [`--init-ci`](#usage) option to add an example [`.gitlab-ci.yml` file](support/.gitlab-ci.example.yml) to your project

### Contributing

Please refer to the [Contributing Guide](CONTRIBUTING.md)
