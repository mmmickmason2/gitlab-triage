module Gitlab
  module Triage
    module CommandBuilders
      class BaseCommandBuilder
        def initialize(items)
          @items = [items].flatten
          @items.delete('')
        end

        def build_command
          if @items.any?
            [slash_command_string, content_string].compact.join(separator)
          else
            ""
          end
        end

        private

        def separator
          ' '
        end

        def content_string
          @items.map do |item|
            format_item(item)
          end.join(separator)
        end
      end
    end
  end
end
