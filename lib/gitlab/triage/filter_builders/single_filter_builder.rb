require_relative 'base_filter_builder'

module Gitlab
  module Triage
    module FilterBuilders
      class SingleFilterBuilder < BaseFilterBuilder
        def filter_content
          filter_contents
        end
      end
    end
  end
end
