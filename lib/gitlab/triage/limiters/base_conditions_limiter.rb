module Gitlab
  module Triage
    module Limiters
      class BaseConditionsLimiter
        def initialize(resource, condition)
          @resource = resource
          validate_condition(condition)
          initialize_variables(condition)
        end

        def self.params_limiter_names(params = nil)
          params ||= limiter_parameters

          params.map do |param|
            param[:name]
          end
        end

        def self.all_params_limiter_names
          params_limiter_names
        end

        def self.params_checking_condition_value
          params_limiter_names params_check_for_field(:values)
        end

        def self.params_checking_condition_type
          params_limiter_names params_check_for_field(:type)
        end

        def self.params_check_for_field(field)
          limiter_parameters.select do |param|
            param[field].present?
          end
        end

        def validate_condition(condition)
          validate_required_parameters(condition)
          validate_parameter_types(condition)
          validate_parameter_content(condition)
        end

        def validate_required_parameters(condition)
          self.class.limiter_parameters.each do |param|
            raise ArgumentError, "#{param[:name]} is a required parameter" unless condition[param[:name]]
          end
        end

        def validate_parameter_types(condition)
          self.class.limiter_parameters.each do |param|
            raise ArgumentError, "#{param[:name]} must be of type #{param[:type]}" unless condition[param[:name]].is_a?(param[:type])
          end
        end

        def validate_parameter_content(condition)
          self.class.limiter_parameters.each do |param|
            if param[:values]
              raise ArgumentError, "#{param[:name]} must be of one of #{param[:values].join(',')}" unless param[:values].include?(condition[param[:name]])
            end
          end
        end
      end
    end
  end
end
