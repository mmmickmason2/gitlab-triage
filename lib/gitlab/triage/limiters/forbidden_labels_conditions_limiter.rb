require_relative 'base_conditions_limiter'

module Gitlab
  module Triage
    module Limiters
      class ForbiddenLabelsConditionsLimiter < BaseConditionsLimiter
        def self.limiter_parameters
          []
        end

        def validate_condition(condition)
          raise ArgumentError, 'condition must be an array containing forbidden label values' unless condition.is_a?(Array)
        end

        def initialize_variables(forbidden_labels)
          @attribute = :labels
          @forbidden_labels = forbidden_labels
        end

        def resource_value
          @resource[@attribute]
        end

        def calculate
          label_intersection.empty?
        end

        private

        def label_intersection
          resource_value & @forbidden_labels
        end
      end
    end
  end
end
