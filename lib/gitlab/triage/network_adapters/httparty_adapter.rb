require 'httparty'

require_relative 'base_adapter'
require_relative '../ui'

module Gitlab
  module Triage
    module NetworkAdapters
      class HttpartyAdapter < BaseAdapter
        def get(token, url)
          response = HTTParty.get(
            url,
            headers: {
              'Content-type' => 'application/json',
              'PRIVATE-TOKEN' => token
            }
          )

          if response.response.is_a?(Net::HTTPUnauthorized)
            puts Gitlab::Triage::UI.debug response.inspect if options.debug
            raise 'The provided token is unauthorized!'
          end

          {
            more_pages: (response.headers["x-next-page"] != ""),
            next_page_url: url + "&page=#{response.headers['x-next-page']}",
            results: response.parsed_response
          }
        end

        def post(token, url, body)
          HTTParty.post(
            url,
            body: { body: body }.to_json,
            headers: {
              'Content-type' => 'application/json',
              'PRIVATE-TOKEN' => token
            }
          )
        end
      end
    end
  end
end
