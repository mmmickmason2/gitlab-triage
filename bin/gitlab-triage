#!/usr/bin/env ruby

require 'optparse'
require 'yaml'
require_relative '../lib/gitlab/triage/engine'

Options = Struct.new(
  :dry_run,
  :policies_file,
  :project_id,
  :token,
  :debug
)

class TriageOptionParser
  class << self
    def parse(argv)
      options = Options.new

      parser = OptionParser.new do |opts|
        opts.banner = "Usage: #{__FILE__} [options]\n\n"

        opts.on('-n', '--dry-run', "Don't actually update anything, just print") do |value|
          options.dry_run = value
        end

        opts.on('-f', '--policies-file [string]', String, 'A valid policies YML file') do |value|
          options.policies_file = value
        end

        opts.on('-p', '--project-id [string]', String, 'A project ID or path') do |value|
          options.project_id = value
        end

        opts.on('-t', '--token [string]', String, 'A valid API token') do |value|
          options.token = value
        end

        opts.on('-d', '--debug', 'Print debug information') do |value|
          options.debug = value
        end

        opts.on('-h', '--help', 'Print help message') do
          $stdout.puts opts
          exit
        end

        opts.on('--init', 'Initialize the project with a policy file') do
          FileUtils.cp('./support/.triage-policies.example.yml', './.triage-policies.yml')
          exit
        end

        opts.on('--init-ci', 'Initialize the project with a .gitlab-ci.yml file') do
          FileUtils.cp('./support/.gitlab-ci.example.yml', './.gitlab-ci.yml')
          exit
        end
      end

      parser.parse!(argv)

      options
    end
  end
end

options = TriageOptionParser.parse(ARGV)

policies_file = options.policies_file || '.triage-policies.yml'
policies = HashWithIndifferentAccess.new(YAML.load_file(policies_file))

Gitlab::Triage::Engine
  .new(policies: policies, options: options)
  .perform
